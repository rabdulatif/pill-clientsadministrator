﻿namespace Root
{
    /// <summary>
    /// 
    /// </summary>
    public class RealTimeQueryViewModel
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sapCode"></param>
        /// <param name="query"></param>
        public RealTimeQueryViewModel(string sapCode, string query)
        {
            SapCode = sapCode;
            Query = query;
        }

        /// <summary>
        /// 
        /// </summary>
        public RealTimeQueryViewModel()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public string SapCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Query { get; set; }
    }
}
