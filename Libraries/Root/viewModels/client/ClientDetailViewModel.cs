﻿using System.Collections.Generic;

namespace Root.viewModels.client
{
    /// <summary>
    /// 
    /// </summary>
    public class ClientDetailViewModel
    {
        /// <summary>
        /// Клиент
        /// </summary>
        public ClientViewModel Client { get; set; }

        /// <summary>
        /// Запросы
        /// </summary>
        public List<CommandViewModel> Commands { get; set; }

        /// <summary>
        /// Сессии
        /// </summary>
        public List<SessionViewModel> Sessions { get; set; }
    }
}
