﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Root
{
    /// <summary>
    /// 
    /// </summary>
    public class ClientViewModel
    {
        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public Guid ClientGuid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Идентификатор соединения")]
        [Browsable(false)]
        public string ConnectionId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("В сети")]
        public bool IsOnline { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Сап-код")]
        public string SapCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Время пинга")]
        [DisplayFormat(DataFormatString = "dd.MM.yyyy hh:mm:ss")]
        [Browsable(false)]
        public DateTime PingTime { get; set; }
        public string PingTimeStr => PingTime.ToString("dd.MM.yyyy hh:mm:ss");

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Последняя активность")]
        [DisplayFormat(DataFormatString = "dd.MM.yyyy hh:mm:ss")]
        public DateTime LastSeen { get; set; }
        public string LastSeenStr => LastSeen.ToString("dd.MM.yyyy hh:mm:ss");

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Время регистрации")]
        [DisplayFormat(DataFormatString = "dd.MM.yyyy hh:mm:ss")]
        public DateTime RegisteredTime { get; set; }
        public string RegisteredTimeStr => RegisteredTime.ToString("dd.MM.yyyy hh:mm:ss");

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Запросы")]
        public int CommandsCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Сессии")]
        public int SessionCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Продолжительность сессии")]
        public string SessionLenght =>
               TimeSpan.FromMinutes((RegisteredTime - LastSeen).TotalMinutes).ToString(@"hh\:mm\:ss");

    }
}
