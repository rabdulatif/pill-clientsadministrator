﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Root
{
    /// <summary>
    /// 
    /// </summary>
    public class SessionTreeViewModel
    {
        /// <summary>
        /// 
        /// </summary>
        public String ClientSapCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<SessionViewModel> Sessions { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int TotalSessions => Sessions.Count;
    }

    /// <summary>
    /// 
    /// </summary>
    public class SessionViewModel
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid SessionGuid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Сап-код")]
        public string ClientSapCode { get; set; }

        /// <summary>
        /// Время регистратции
        /// </summary>
        [DisplayName("Время регистратции")]
        public DateTime RegisteredTime { get; set; }
        public string RegisteredTimeStr => RegisteredTime.ToString("dd.MM.yyyy hh:mm:ss");

        /// <summary>
        /// Последное активность
        /// </summary>
        [DisplayName("Последное активность")]
        public DateTime DisconnectTime { get; set; }
        public string DisconnectTimeStr => DisconnectTime.ToString("dd.MM.yyyy hh:mm:ss");

        /// <summary>
        /// Продолжительность сеанса
        /// </summary>
        [DisplayName("Продолжительность сеанса")]
        public string SessionLenght =>
               TimeSpan.FromMinutes((RegisteredTime - DisconnectTime).TotalMinutes).ToString(@"hh\:mm\:ss");
    }

}
