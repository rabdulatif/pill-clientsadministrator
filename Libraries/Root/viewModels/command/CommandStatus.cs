﻿namespace Root
{
    public enum CommandStatus
    {
        Send = 100,
        ExecuteSuccess = 200,
        ExecuteFail = 500
    }
}
