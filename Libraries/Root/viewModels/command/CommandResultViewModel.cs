﻿using System;

namespace Root
{
    /// <summary>
    /// 
    /// </summary>
    public class CommandResultViewModel
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid CommandGuid { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string ClientSapCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AdminGuid { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public dynamic Content { get; set; }

        /// <summary>
        ///
        /// </summary>
        public CommandStatus Status { get; set; }
    }
}
