﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Root
{
    /// <summary>
    /// 
    /// </summary>
    public class CommandViewModel
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid CommandGuid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AdminGuid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Display(Name ="Наименование запроса")]
        public string CommandName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Запрос")]
        [DataType(DataType.MultilineText)]
        public string CommandQuery { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<string> SapCodes { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Статус")]
        public CommandStatus Status { get; set; }
        public string StatusStr => Status.ToString();

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Результат")]
        public string Result { get; set; }

        /// <summary>
        ///
        /// </summary>
        [DisplayName("Отправленное время")]
        public DateTime SendTime { get; set; }
        public string SendTimeStr => SendTime.ToString("dd.MM.yyyy hh:mm:ss");

        /// <summary>
        /// 
        /// </summary>
        [DisplayName("Время выполнение")]
        public DateTime? ExecutedTime { get; set; }
        public string ExecutedTimeStr => ExecutedTime?.ToString("dd.MM.yyyy hh:mm:ss");
    }
}
