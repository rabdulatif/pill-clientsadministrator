﻿using System.Data.SqlClient;
using Dapper;

namespace PharmacySignalR.Client.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class CommandService : ICommandService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="commandQuery"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        public dynamic Execute(string commandQuery, string dbConnection)
        {
            var connection = new SqlConnection(dbConnection);
            connection.Open();

            return connection.Query(commandQuery);
        }
    }
}
