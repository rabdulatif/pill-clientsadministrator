﻿namespace PharmacySignalR.Client.Services
{
    internal interface ICommandService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="commandQuery"></param>
        /// <param name="dbConnection"></param>
        /// <returns></returns>
        dynamic Execute(string commandQuery, string dbConnection);
    }
}
