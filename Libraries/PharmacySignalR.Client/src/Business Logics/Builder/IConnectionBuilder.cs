﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using Root;

namespace PharmacySignalR.Client
{
    /// <summary>
    /// 
    /// </summary>
    public interface IConnectionBuilder
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="endpointUri"></param>
        /// <returns></returns>
        IConnectionBuilder SetEndpointUrl(string endpointUri);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        IConnectionBuilder SetConnectionString(string connectionString);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="hubName"></param>
        /// <returns></returns>
        IConnectionBuilder SetHubName(string hubName);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        IConnectionBuilder SetToken(string token);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        IConnectionBuilder SetClient(ClientViewModel client);
        /// <summary>
        /// 
        /// </summary>
        ConnectionState ConnectionState { get; }

        /// <summary>
        /// Подключение к Hub API
        /// </summary>
        /// <returns>Task<string> result</returns>
        Task<string> ConnectAsync();

        /// <summary>
        /// Отключиться от Hub API
        /// </summary>
        void Disconnect();
    }
}
