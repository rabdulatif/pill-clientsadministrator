﻿using System;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using PharmacySignalR.Client.Services;
using Root;

namespace PharmacySignalR.Client
{
    /// <summary>
    /// 
    /// </summary>
    public class ConnectionBuilder : IConnectionBuilder
    {
        #region Settings
        /// <summary>
        /// 
        /// </summary>
        private readonly CommandService _commandService;

        /// <summary>
        /// 
        /// </summary>
        private string _endpointUrl;
        
        /// <summary>
        /// 
        /// </summary>
        private string _hubName;
        
        /// <summary>
        /// 
        /// </summary>
        private string _connectionString;
        
        /// <summary>
        /// 
        /// </summary>
        private string _connectionResult;
        
        /// <summary>
        /// 
        /// </summary>
        private string _token;

        /// <summary>
        /// 
        /// </summary>
        private HubConnection _connection;
        
        /// <summary>
        /// 
        /// </summary>
        private IHubProxy _proxy;
        
        /// <summary>
        /// 
        /// </summary>
        private ClientViewModel _client;

        /// <summary>
        /// 
        /// </summary>
        public ConnectionState ConnectionState { get; private set; } = ConnectionState.Disconnected;

        #endregion

        #region Ctor and fluent settings

        /// <summary>
        /// 
        /// </summary>
        public ConnectionBuilder()
        {
            _commandService = new CommandService();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="endpointUri"></param>
        /// <returns></returns>
        public IConnectionBuilder SetEndpointUrl(string endpointUri)
        {
            _endpointUrl = endpointUri;
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public IConnectionBuilder SetConnectionString(string connectionString)
        {
            _connectionString = connectionString;
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hubName"></param>
        /// <returns></returns>
        public IConnectionBuilder SetHubName(string hubName)
        {
            _hubName = hubName;
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public IConnectionBuilder SetToken(string token)
        {
            _token = token;
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public IConnectionBuilder SetClient(ClientViewModel client)
        {
            _client = client;
            return this;
        }

        #endregion

        #region Connection life cycle

        /// <summary>
        /// Подключение к Hub API
        /// </summary>
        /// <returns></returns>
        public async Task<string> ConnectAsync()
        {
            Validate();
            InitializeFields();
            RegisterConnectionEvents();

            _proxy?.On<CommandViewModel>("serverCommand", (command) =>
            {
                var queryRes = OnServerCommand(command);
                _proxy?.Invoke("ClientMessage", new CommandResultViewModel
                {
                    CommandGuid = command.CommandGuid,
                    AdminGuid = command.AdminGuid,
                    ClientSapCode = _client.SapCode,
                    Content = queryRes,
                    Status = command.Status
                });
            });

            _proxy?.On<CommandViewModel>("realTimeQuery", (command) =>
            {
                var result = OnServerCommand(command);
                _proxy?.Invoke("RealTimeQueryResult", new CommandResultViewModel
                {
                    CommandGuid = command.CommandGuid,
                    AdminGuid = command.AdminGuid,
                    ClientSapCode = _client.SapCode,
                    Content = result,
                    Status = command.Status
                });
            });

            _proxy?.On("onReconnected", (connectionId) =>
            {
                _proxy?.Invoke("Register", _client);
            });

            _proxy.On("pong", () =>
            {
                var sapCode = _client.SapCode;
                _proxy?.Invoke("OnPong", sapCode);
            });

            await _connection.Start().ContinueWith(task =>
            {
                _connectionResult = task.IsFaulted
                    ? $"Failed to start: {task?.Exception?.GetBaseException()}"
                    : "Connection Started";
            });

            if (_connection.State == ConnectionState.Connected)
            {
                ConnectionState = ConnectionState.Connected;
                await _proxy?.Invoke("Register", _client);
            }

            return _connectionResult;
        }

        /// <summary>
        /// 
        /// </summary>
        private void Validate()
        {
            if (string.IsNullOrEmpty(_endpointUrl))
                throw new InvalidOperationException("SetEndpoint() must be called before you connect");
            if (string.IsNullOrEmpty(_token))
                throw new InvalidOperationException("SetToken() must be called before you connect");
            if (string.IsNullOrEmpty(_connectionString))
                throw new InvalidOperationException("SetConnectionString() must be called before you connect");
            if (string.IsNullOrEmpty(_hubName))
                throw new InvalidOperationException("SetHubName() must be called before you connect");
            if (_client == null)
                throw new InvalidOperationException("SetClient() must be called before you connect");
            if (_connection != null && _connection.State == ConnectionState.Connected)
                throw new InvalidOperationException("Connection not disconnected");
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        private void InitializeFields()
        {
            _connection = new HubConnection(_endpointUrl);
            _connection.Headers.Add("Authorization", new AuthenticationHeaderValue(
                "Bearer", _token).ToString());

            _proxy = _connection.CreateHubProxy(_hubName);

        }

        /// <summary>
        /// 
        /// </summary>
        private void RegisterConnectionEvents()
        {
            if (_connection == null) return;

            _connection.Reconnected += Connection_Reconnected;
            _connection.Closed += Connection_Closed;
        }

        /// <summary>
        /// 
        /// </summary>
        private async void Connection_Closed()
        {
            try
            {
                ConnectionState = ConnectionState.Disconnected;
                DismissCurrentConnection();
                //await Task.Delay(new Random().Next(0, 15) * 1000);
                await ConnectAsync();
            }
            catch (Exception)
            {
                ConnectionState = ConnectionState.Disconnected;
                DismissCurrentConnection();
                //await Task.Delay(new Random().Next(0, 20) * 1000);
                await ConnectAsync();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private async void Connection_Reconnected()
        {
            await _proxy.Invoke("Register", _client);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        private dynamic OnServerCommand(CommandViewModel command)
        {
            try
            {
                if (command == null)
                {
                    command.Status = CommandStatus.ExecuteFail;
                    return "Command model is null";
                }

                var res =  _commandService.Execute(command.CommandQuery, _connectionString);
                command.Status = CommandStatus.ExecuteSuccess;
                return res;
            }
            catch (Exception ex)
            {
                command.Status = CommandStatus.ExecuteFail;
                return $"Exception:{ex.Message}";
            }
        }

        /// <summary>
        /// Отключиться от Hub API
        /// </summary>
        public void Disconnect()
        {
            DismissCurrentConnection();
            _proxy = null;
        }

        /// <summary>
        /// 
        /// </summary>
        protected void DismissCurrentConnection()
        {
            if (_connection == null) return;
            _connection.Reconnected -= Connection_Reconnected;
            _connection.Closed -= Connection_Closed;

            var connectionToDispose = _connection;
            _connection = null;

            connectionToDispose?.Dispose();
        }
    }
}
