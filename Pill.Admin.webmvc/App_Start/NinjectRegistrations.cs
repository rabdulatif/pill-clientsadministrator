﻿using Ninject.Modules;
using Pill.Admin.webmvc.Logics.Services;
using Pill.Admin.webmvc.Logics.Services.Interfaces;

namespace Pill.Admin.webmvc.App_Start
{
    /// <summary>
    /// 
    /// </summary>
    public class NinjectRegistrations : NinjectModule
    {
        /// <summary>
        /// 
        /// </summary>
        public override void Load()
        {
            Bind<IClientService>().To<ClientService>();
            Bind<IClientSessionService>().To<ClientSessionService>();
            Bind<ICommandService>().To<CommandService>();
        }
    }
}