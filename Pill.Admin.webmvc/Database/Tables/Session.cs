﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pill.Admin.webmvc.Database
{
    /// <summary>
    /// 
    /// </summary>
    [Table("Session")]
    public class Session
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid SessionGuid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ForeignKey(nameof(Client))]
        public Guid ClientGuid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Client Client { get; set; }

        /// <summary>
        /// Pегистрированное время
        /// </summary>
        public DateTime RegisteredTime { get; set; }

        /// <summary>
        /// Время завершения последнего подключения
        /// </summary>
        public DateTime DisconnectTime { get; set; }

        /// <summary>
        /// Продолжительность сеанса
        /// </summary>
        public string SessionLenght =>
               TimeSpan.FromMinutes((RegisteredTime - DisconnectTime).TotalMinutes).ToString(@"hh\:mm\:ss");
    }

}