﻿using Root;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pill.Admin.webmvc.Database
{
    public class Command
    {
        /// <summary>
        /// 
        /// </summary>
        //public Command()
        //{
        //    Clients = new HashSet<Client>();
        //}

        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Browsable(false)]
        public Guid CommandGuid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AdminGuid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Display(Name ="Наименование запроса")]
        public string CommandName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Display(Name ="Запрос")]
        [DataType(DataType.MultilineText)]
        public string CommandQuery { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public CommandStatus Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        ///
        /// </summary>
        public DateTime SendTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? ExecutedTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual ICollection<Client> Clients { get; set; }
    }
}