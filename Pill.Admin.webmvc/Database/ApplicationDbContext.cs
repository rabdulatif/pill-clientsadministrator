﻿using Microsoft.AspNet.Identity.EntityFramework;
using Pill.Admin.webmvc.Models;
using System.Data.Entity;

namespace Pill.Admin.webmvc.Database
{
    /// <summary>
    /// 
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        /// <summary>
        /// 
        /// </summary>
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            //System.Data.Entity.Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Client>().
                HasMany<Command>(c => c.Commands).
                WithMany(s => s.Clients).
                Map(
                m =>
                {
                    m.MapLeftKey("ClientGuid");
                    m.MapRightKey("CommandGuid");
                    m.ToTable("ClientCommands");
                });
        }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<Client> Clients { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<Session> Sessions { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<Command> Commands { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}