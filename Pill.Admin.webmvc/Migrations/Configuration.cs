using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Pill.Admin.webmvc.Models;
using System.Data.Entity.Migrations;

namespace Pill.Admin.webmvc.Migrations
{
    /// <summary>
    /// 
    /// </summary>
    internal sealed class Configuration : DbMigrationsConfiguration<Database.ApplicationDbContext>
    {
        /// <summary>
        /// 
        /// </summary>
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "Pill.Admin.webmvc.Database.ApplicationDbContext";
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        protected override void Seed(Database.ApplicationDbContext context)
        {
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            var adminRole = new IdentityRole { Name = Roles.AdminRoleName };
            var hubAdmin = new IdentityRole { Name = Roles.HubAdminRoleName };

            if (!roleManager.RoleExists(adminRole.Name))
                roleManager.Create(adminRole);
            if (!roleManager.RoleExists(hubAdmin.Name))
                roleManager.Create(hubAdmin);

            var admin = new ApplicationUser { SapCode = "TAdmin", UserName = "TAdmin", IsActive = true };
            string password = "P@ssw0rd";
            
            if (userManager.FindByName(admin.UserName) != null)
                return;

            var result = userManager.Create(admin, password);

            if (result.Succeeded)
            {
                userManager.AddToRole(admin.Id, adminRole.Name);
                userManager.AddToRole(admin.Id, hubAdmin.Name);
            }

            var user = new ApplicationUser { SapCode = "User", UserName = "User" };
            
            if (userManager.FindByName(user.UserName) != null)
                return;

            var res = userManager.Create(user, "123456");
            if (res.Succeeded)
                userManager.AddToRole(user.Id, hubAdmin.Name);

            base.Seed(context);
        }

    }
}
