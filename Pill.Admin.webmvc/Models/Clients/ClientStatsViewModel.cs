﻿namespace Pill.Admin.webmvc.Models.Clients
{
    /// <summary>
    /// 
    /// </summary>
    public class ClientStatsViewModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int TotalUsers { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int OnlineClients { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int OfflineClients { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int TotalCommands { get; set; }
    }
}