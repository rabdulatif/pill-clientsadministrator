﻿using System.ComponentModel.DataAnnotations;

namespace Pill.Admin.webmvc.Models.Users
{
    /// <summary>
    /// 
    /// </summary>
    public class EditUserViewModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string SapCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsActive { get; set; }
    }
}