﻿using System.ComponentModel.DataAnnotations;

namespace Pill.Admin.webmvc.Models.Users
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateUserViewModel
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string SapCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsActive { get; set; }
    }
}