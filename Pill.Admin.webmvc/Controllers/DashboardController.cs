﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using Pill.Admin.webmvc.Database;
using Pill.Admin.webmvc.Extensions;
using Pill.Admin.webmvc.Hubs;
using Pill.Admin.webmvc.Logics.Services;
using Pill.Admin.webmvc.Logics.Services.Interfaces;
using Root;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Pill.Admin.webmvc.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [System.Web.Mvc.Authorize(Roles = Models.Roles.HubAdminRoleName + "," + Models.Roles.AdminRoleName)]
    public class DashboardController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        private ApplicationUserManager _userManager;

        /// <summary>
        /// 
        /// </summary>
        private ICommandService _commandService;

        /// <summary>
        /// 
        /// </summary>
        private IClientService _clientService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userManager"></param>
        public DashboardController(ApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commandService"></param>
        public DashboardController(ICommandService commandService, IClientService clientService)
        {
            _commandService = commandService;
            _clientService = clientService;
        }

        /// <summary>
        /// 
        /// </summary>
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult GetClients()
        {
            var clients = _clientService.TryGetAllClientDetails().ToClientViewModels();
            return Json(new { data = clients }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Clients(ManageMessage? message)
        {
            ViewBag.ResultMessage = message == ManageMessage.QuerySaved ? "Запрос сохранен" : "";
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult ClientList()
        {
            return Json(new { data = HubConnectionAPI.ClientList.Count }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sapCode"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ClientDetails(string sapCode)
        {
            if (string.IsNullOrEmpty(sapCode))
                return HttpNotFound();

            var clientDetail = _clientService.TryGetClientDetails(sapCode).ToClientDetails();
            return View(clientDetail);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sapCodes"></param>
        /// <returns></returns>
        public PartialViewResult SendMessage(string sapCodes)
        {
            var resSapCodes = JsonConvert.DeserializeObject<List<string>>(sapCodes);
            var cmdViewModel = new CommandViewModel { SapCodes = resSapCodes };
            return PartialView(cmdViewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SendMessage(CommandViewModel viewModel)
        {
            if (ModelState.IsValid == false)
                return View(viewModel);

            var command = new Command
            {
                CommandGuid = Guid.NewGuid(),
                CommandName = viewModel.CommandName,
                CommandQuery = viewModel.CommandQuery,
                SendTime = DateTime.Now,
                Status = CommandStatus.Send
            };
            _commandService.AddEdit(command);

            var sapCodes = viewModel.SapCodes.DeserializeListString();
            HubConnectionAPI.SendCommandToClients(sapCodes, command);

            return RedirectToAction("Clients", new { message = ManageMessage.QuerySaved });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult ClientNewQuery(string sapCode)
        {
            var userId = User.Identity.GetUserId();
            var currentUser = UserManager.FindById(userId);
            if (currentUser == null)
                return LogOff();

            var cmdViewModel = new RealTimeQueryViewModel(sapCode,"");
            return PartialView(cmdViewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            return RedirectToAction("Index", "Dashboard");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public enum ManageMessage
    {
        QuerySaved
    }
}