﻿using Microsoft.AspNet.SignalR;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin;
using Owin;
using Pill.Admin.webmvc.Helpers;
using System;
using System.Text;
using System.Threading.Tasks;

[assembly: OwinStartupAttribute(typeof(Pill.Admin.webmvc.Startup))]
namespace Pill.Admin.webmvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            AppDomain.CurrentDomain.FirstChanceException += CurrentDomain_FirstChanceException;

            ConfigureAuth(app);
            ConfigureJWTAuth(app);
            ConfigureSignalR(app);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrentDomain_FirstChanceException(object sender, System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs e)
        {
            Task.Run(() => LogHelper.Error(e.Exception));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        private void ConfigureJWTAuth(IAppBuilder app)
        {
            app.UseJwtBearerAuthentication(new Microsoft.Owin.Security.Jwt.JwtBearerAuthenticationOptions
            {
                TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("HawNkdgqDvrSluzEIKi0JPXC9UHriMP0")),
                    ValidateIssuer = false,
                    ValidateAudience = false
                }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        private void ConfigureSignalR(IAppBuilder app)
        {
            GlobalHost.Configuration.ConnectionTimeout = TimeSpan.FromSeconds(110);
            GlobalHost.Configuration.DisconnectTimeout = TimeSpan.FromSeconds(30);
            GlobalHost.Configuration.KeepAlive = TimeSpan.FromSeconds(10);
            GlobalHost.Configuration.MaxIncomingWebSocketMessageSize = 256;
            GlobalHost.HubPipeline.RequireAuthentication();

            ConfigureSignalR(GlobalHost.DependencyResolver, GlobalHost.HubPipeline);

            var config = new HubConfiguration()
            {
                EnableDetailedErrors = true
            };

            app.MapSignalR(config);
        }
    }
}
