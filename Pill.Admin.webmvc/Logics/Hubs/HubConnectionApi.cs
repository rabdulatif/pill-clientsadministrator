﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using System.Web.Mvc;
using Pill.Admin.webmvc.Extensions;
using Microsoft.AspNet.SignalR;
using Pill.Admin.webmvc.Helpers;
using Root;
using Pill.Admin.webmvc.Database;
using Newtonsoft.Json;

namespace Pill.Admin.webmvc.Hubs
{
    /// <summary>
    /// 
    /// </summary>
#if !DEBUG
    [Microsoft.AspNet.SignalR.Authorize()]
#else
    [AllowAnonymous]
#endif
    public partial class HubConnectionAPI : Hub
    {
        /// <summary>
        /// 
        /// </summary>
        private static Boolean IsInitialized = false;

        /// <summary>
        /// 
        /// </summary>
        public HubConnectionAPI()
        {
            InitializeClientList();
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitializeClientList()
        {
            try
            {
                ThreadSafeInitialize();
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ThreadSafeInitialize()
        {
            lock (locker)
            {
                if (IsInitialized || ClientList.Count > 0)
                {
                    return;
                }

                var clients = _clientService.TryGetAllClientDetails().ToClientViewModels();
                clients.ForEach(InternalInitialize);

                //Ping();
                IsInitialized = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="client"></param>
        private void InternalInitialize(ClientViewModel client)
        {
            if (ClientList.Exists(c => c.SapCode == client.SapCode) == false)
                ClientList.Add(client);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override Task OnReconnected()
        {
            try
            {
                UpdateOnReconnected();
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex);
            }
            return base.OnReconnected();
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateOnReconnected()
        {
            var client = ClientList.Find(s => s.ConnectionId == Context.ConnectionId);
            if (client != null)
            {
                ClientList.Remove(client);

                client.LastSeen = DateTime.Now;
                client.PingTime = DateTime.Now;
                ClientList.Add(client);
                AddOrUpdateUserInDb(client.ToClient());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stopCalled"></param>
        /// <returns></returns>
        public override Task OnDisconnected(bool stopCalled)
        {
            try
            {
                DisconnectClient();
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex);
            }

            return base.OnDisconnected(stopCalled);
        }

        /// <summary>
        /// 
        /// </summary>
        private void DisconnectClient()
        {
            var client = ClientList.Find(s => s.ConnectionId == Context.ConnectionId);
            if (client != null)
            {
                ClientList.SetClientState(client.SapCode, false);
                _clientService.RemoveClient(client.SapCode);

                var dbClient = _clientService.GetClientBySapCode(client.SapCode);
                if (dbClient == null)
                    return;
                _sessionService.AddOrUpdateSession(new Session
                {
                    RegisteredTime = client.RegisteredTime,
                    DisconnectTime = client.LastSeen,
                    ClientGuid = dbClient.ClientGuid
                });

                Clients.All.adminOnDisconnected(client.SapCode);
            }
        }

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="client"></param>
        public void Register(ClientViewModel client)
        {
            lock (locker)
            {
                if (String.IsNullOrEmpty(client.SapCode))
                    return;
                client.ConnectionId = Context.ConnectionId;
                client.RegisteredTime = DateTime.Now;
                client.PingTime = DateTime.Now;
                client.LastSeen = DateTime.Now;
                client.IsOnline = true;

                ClientList.RemoveAll(r => r.SapCode == client.SapCode);
                ClientList.Add(client);

                AddOrUpdateUserInDb(client.ToClient());
                Clients.All.adminOnConnected(client.SapCode);
            }
        }


        #region PingClients

        public void Ping()
        {
            if (timer != null)
                return;
            timer = new Timer(30000);
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        private static bool canElapse = true;
        private async void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (canElapse == false)
                return;

            try
            {
                canElapse = false;
                if (ClientList.Count > 0)
                {
                    ClientList.ForEach((client) => PongClients(ref client));
                    await Task.Delay(10000);
                    ClientList.ForEach((client) => TryCheckPingTimeout(ref client));
                    Clients.All.onlineClients();
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex);
            }
            canElapse = true;
        }

        private static bool canElapse2 = true;
        public void InternalPong()
        {
            if (canElapse == false)
                return;

            try
            {
                canElapse = false;
                if (ClientList.Count > 0)
                {
                    ClientList.ForEach((client) => PongClients(ref client));
                    Task.Delay(10000).Wait();
                    ClientList.ForEach((client) => TryCheckPingTimeout(ref client));
                    Clients.All.onlineClients();
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex);
            }
            canElapse = true;
        }

        public void PongClients(ref ClientViewModel client)
        {
            ClientList.SetClientPingTime(client.SapCode, DateTime.Now);
            Clients.Client(client.ConnectionId).Pong();
            AddOrUpdateUserInDb(client.SapCode);
        }

        private void TryCheckPingTimeout(ref ClientViewModel client)
        {
            var diffInSeconds = (client.PingTime - client.LastSeen).TotalSeconds;
            if (Math.Abs(diffInSeconds) > 10 && client.IsOnline == true)
            {
                ClientList.SetClientState(client.SapCode, false);
                AddOrUpdateUserInDb(client.SapCode);
                AddUpdateSessionInDb(client.SapCode);
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public void OnPong(string sapCode)
        {
            try
            {
                ClientList.SetClientLastActiveTime(sapCode, DateTime.Now);
                ClientList.SetClientState(sapCode, true);
                AddOrUpdateUserInDb(sapCode);
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sapCode"></param>
        private void AddOrUpdateUserInDb(string sapCode)
        {
            var client = ClientList.FirstOrDefault(s => s.SapCode == sapCode);
            if (client == null)
                return;
            lock (locker)
            {
                _clientService.AddOrUpdateClient(client.ToClient());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="client"></param>
        private void AddOrUpdateUserInDb(Client client)
        {
            if (client == null)
                return;
            lock (locker)
            {
                _clientService.AddOrUpdateClient(client);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sapCode"></param>
        private void AddUpdateSessionInDb(string sapCode)
        {
            var client = _clientService.GetClientBySapCode(sapCode);
            if (client == null)
                return;
            lock (locker)
            {
                _sessionService.AddOrUpdateSession(new Session
                {
                    ClientGuid = client.ClientGuid,
                    RegisteredTime = client.RegisteredTime,
                    DisconnectTime = client.LastSeen
                });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sapCodes"></param>
        /// <param name="cmd"></param>
        public static void SendCommandToClients(List<string> sapCodes, Command cmd)
        {
            //if (string.IsNullOrEmpty(cmd.AdminGuid))
            //    return;

            var commandViewModel = cmd.ToCommandViewModel();
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<HubConnectionAPI>();
            sapCodes.ForEach(s => SendCommandToClients(s, hubContext, commandViewModel));

            //hubContext.Clients.Clients(sapCodes).serverCommand(commandViewModel);
            _commandService.AddEdit(cmd);
            sapCodes.ForEach(sapCode => { AddCommandClient(cmd.CommandGuid, sapCode); });

            //Clients.Clients(connectionIds).serverCommand(commandViewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sapCode"></param>
        /// <param name="hubContext"></param>
        private static void SendCommandToClients(string sapCode, IHubContext hubContext, CommandViewModel command)
        {
            var client = ClientList.FirstOrDefault(f => f.SapCode == sapCode);
            if (client == null)
                return;

            hubContext.Clients.Client(client.ConnectionId).serverCommand(command);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sapCode"></param>
        /// <param name="query"></param>
        public void SendRealTimeQuery(string sapCode, string query)
        {
            var client = ClientList.FirstOrDefault(f => f.SapCode == sapCode);
            if (client == null)
                return;

            var command = new Command
            {
                CommandGuid = Guid.NewGuid(),
                CommandName = $"({DateTime.Now}) RealTimeQuery",
                CommandQuery = query,
                SendTime = DateTime.Now,
                Status = CommandStatus.Send
            };

            _commandService.AddEdit(command);
            AddCommandClient(command.CommandGuid, sapCode);

            var hubContext = GlobalHost.ConnectionManager.GetHubContext<HubConnectionAPI>();
            hubContext.Clients.Client(client.ConnectionId).realTimeQuery(command.ToCommandViewModel());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commandGuid"></param>
        /// <param name="sapCode"></param>
        private static void AddCommandClient(Guid commandGuid, string sapCode)
        {
            var cmd = _commandService.GetCommand(commandGuid);
            if (cmd == null)
                return;

            var client = _clientService.GetClientBySapCode(sapCode);
            if (client == null)
                return;

            _commandService.AddCommandClient(cmd, client);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commandResult"></param>
        public void ClientMessage(CommandResultViewModel commandResult)
        {
            var command = GetCommandFromClientResult(commandResult);
            var client = _clientService.GetClientBySapCode(commandResult.ClientSapCode);

            _commandService.AddEdit(command);
            _commandService.AddCommandClient(command, client);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="queryResult"></param>
        public void RealTimeQueryResult(CommandResultViewModel queryResult)
        {
            var command = GetCommandFromClientResult(queryResult);
            var client = _clientService.GetClientBySapCode(queryResult.ClientSapCode);

            _commandService.AddEdit(command);
            _commandService.AddCommandClient(command, client);

            Clients.All.adminOnQueryResultReceived(command.ToCommandViewModel());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commandResult"></param>
        /// <returns></returns>
        private Command GetCommandFromClientResult(CommandResultViewModel commandResult)
        {
            var command = _commandService.GetCommand(commandResult.CommandGuid);
            if (command == null)
                return null;

            command.ExecutedTime = DateTime.Now;
            command.Status = commandResult.Status;
            command.Result = Convert.ToString(commandResult.Content);

            return command;
        }
        #endregion
    }
}