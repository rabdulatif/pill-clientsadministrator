﻿using Pill.Admin.webmvc.Database;
using Pill.Admin.webmvc.Logics.Services;
using Pill.Admin.webmvc.Logics.Services.Interfaces;
using Root;
using System.Collections.Generic;
using System.Timers;

namespace Pill.Admin.webmvc.Hubs
{
    /// <summary>
    /// 
    /// </summary>
    public partial class HubConnectionAPI
    {
        /// <summary>
        /// 
        /// </summary>
        static object locker = new object();

        /// <summary>
        /// 
        /// </summary>
        private Timer timer;

        /// <summary>
        /// 
        /// </summary>
        private static IClientService _clientService = new ClientService();

        /// <summary>
        /// 
        /// </summary>
        private static IClientSessionService _sessionService = new ClientSessionService();

        /// <summary>
        /// 
        /// </summary>
        private static ICommandService _commandService = new CommandService();

        /// <summary>
        /// 
        /// </summary>
        public static List<ClientViewModel> ClientList { get; private set; } = new List<ClientViewModel>();
    }
}