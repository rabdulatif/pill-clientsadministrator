﻿using Pill.Admin.webmvc.Database;
using Root;
using System.Collections.Generic;

namespace Pill.Admin.webmvc.Logics.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class SessionExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sessions"></param>
        /// <returns></returns>
        public static List<SessionViewModel> ToSessionViewModel(this ICollection<Session> sessions)
        {
            var sessionsModel = new List<SessionViewModel>();

            foreach (var item in sessions)
                sessionsModel.Add(item.ToSessionViewModel());

            return sessionsModel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public static SessionViewModel ToSessionViewModel(this Session session)
        {
            return new SessionViewModel
            {
                SessionGuid=session.SessionGuid,
                ClientSapCode=session.Client?.SapCode,
                RegisteredTime=session.RegisteredTime,
                DisconnectTime=session.DisconnectTime
            };
        }
    }
}