﻿using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Pill.Admin.webmvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pill.Admin.webmvc.Logics.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class ApplicationUserManagerExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="users"></param>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public static List<ApplicationUser> GetUsersInRole(this ApplicationUserManager manager, string roleName)
        {
            List<ApplicationUser> roleUsers = new List<ApplicationUser>();

            foreach (var user in manager.Users)
            {
                if (manager.IsInRole(user.Id, roleName))
                {
                    roleUsers.Add(user);
                }
            }

            return roleUsers;
        }
    }
}