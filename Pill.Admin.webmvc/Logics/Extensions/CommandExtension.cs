﻿using Pill.Admin.webmvc.Database;
using Root;
using System.Collections.Generic;
using System.Linq;

namespace Pill.Admin.webmvc.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class CommandExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="commands"></param>
        /// <returns></returns>
        public static List<CommandViewModel> ToCommandViewModel(this ICollection<Command> commands)
        {
            var commandsModel = new List<CommandViewModel>();

            foreach (var item in commands)
                commandsModel.Add(item.ToCommandViewModel());

            return commandsModel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public static CommandViewModel ToCommandViewModel(this Command command)
        {
            return new CommandViewModel
            {
                CommandGuid = command.CommandGuid,
                CommandName = command.CommandName,
                CommandQuery = command.CommandQuery,
                Status = command.Status,
                AdminGuid = command.AdminGuid,
                //ConnectionIds = command.Clients?.Select(s => s.ConnectionId).ToList(),
                SendTime = command.SendTime,
                ExecutedTime=command.ExecutedTime,
                Result=command.Result
            };
        }
    }
}