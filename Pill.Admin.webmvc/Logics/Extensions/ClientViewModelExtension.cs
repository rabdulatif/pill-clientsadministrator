﻿using Pill.Admin.webmvc.Database;
using Root;

namespace Pill.Admin.webmvc.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class ClientViewModelExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientViewModel"></param>
        /// <returns></returns>
        public static Client ToClient(this ClientViewModel clientViewModel)
        {
            return new Client
            {
                ClientGuid = clientViewModel.ClientGuid,
                ConnectionId = clientViewModel.ConnectionId,
                IsOnline = clientViewModel.IsOnline,
                SapCode = clientViewModel.SapCode,
                PingTime = clientViewModel.PingTime,
                LastSeen = clientViewModel.LastSeen,
                RegisteredTime = clientViewModel.RegisteredTime
            };
        }
    }
}