﻿using Pill.Admin.webmvc.Database;
using Pill.Admin.webmvc.Logics.Extensions;
using Root;
using Root.viewModels.client;
using System.Collections.Generic;

namespace Pill.Admin.webmvc.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class ClientExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clients"></param>
        /// <returns></returns>
        public static List<ClientViewModel> ToClientViewModels(this IEnumerable<Client> clients)
        {
            List<ClientViewModel> viewModelList = new List<ClientViewModel>();

            foreach (var item in clients)
                viewModelList.Add(item.ToClientViewModel());

            return viewModelList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public static ClientDetailViewModel ToClientDetails(this Client client)
        {
            return new ClientDetailViewModel
            {
                Client = client.ToClientViewModel(),
                Commands = client.Commands?.ToCommandViewModel(),
                Sessions = client.Sessions?.ToSessionViewModel()
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clients"></param>
        /// <returns></returns>
        public static ClientViewModel ToClientViewModel(this Client client)
        {
            var model = new ClientViewModel
            {
                ClientGuid = client.ClientGuid,
                SapCode = client.SapCode,
                ConnectionId = client.ConnectionId,
                IsOnline = client.IsOnline,
                PingTime = client.PingTime,
                LastSeen = client.LastSeen,
                RegisteredTime = client.RegisteredTime,
                CommandsCount = client.Commands?.Count ?? 0,
                SessionCount = client.Sessions?.Count?? 0
            };
            return model;
        }
    }
}