﻿using static Pill.Admin.webmvc.Controllers.UsersController;

namespace Pill.Admin.webmvc.Logics.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class UserMessageIdTranslator
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string TranslateToText(this ManageMessageId? message)
        {
            return message == ManageMessageId.ChangePasswordSuccess ? "Пароль успешно изменено."
              : message == ManageMessageId.SetPasswordSuccess ? "Пароль успешно установлено."
              : message == ManageMessageId.RemoveLoginSuccess ? "Пользователь успешно удален."
              : message == ManageMessageId.Error ? "Произошла ошибка"
              : "";
        }
    }
}