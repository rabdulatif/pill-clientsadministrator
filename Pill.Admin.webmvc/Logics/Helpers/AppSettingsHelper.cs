﻿using System;
using System.Configuration;
using System.IO;
using System.Text;

namespace Pill.Admin.webmvc.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class AppSettingsHelper
    {
        /// <summary>
        /// 
        /// </summary>
        private static string LocalAppPath { get; set; }

        /// <summary>
        /// 
        /// </summary>
        static AppSettingsHelper()
        {
            LocalAppPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\PillAdminka.config";
            CreateFileIfNotExists();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetAdminHardwareID(string sapCode)
        {
            try
            {
                var configFileMap = new ExeConfigurationFileMap
                {
                    ExeConfigFilename = LocalAppPath
                };

                var config = ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);
                var id = config.AppSettings.Settings[$"{sapCode}Guid"]?.Value;
                if (string.IsNullOrEmpty(id))
                {
                    SetSetting($"{sapCode}Guid", Guid.NewGuid().ToString());
                    return GetAdminHardwareID(sapCode);
                }
                return id;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        static void CreateFileIfNotExists()
        {
            if (!File.Exists(LocalAppPath))
            {
                using (File.Create(LocalAppPath))
                {
                }
                InternalSetConfigRootElements();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        static void InternalSetConfigRootElements()
        {
            var sb = new StringBuilder();
            sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
            sb.AppendLine("<configuration>");
            sb.AppendLine("</configuration>");
            File.WriteAllText(LocalAppPath, sb.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SetSetting(string key, string value)
        {
            var configFileMap = new ExeConfigurationFileMap { ExeConfigFilename = LocalAppPath };
            var config = ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);

            if (config.AppSettings.Settings[key] == null)
                config.AppSettings.Settings.Add(key, value);
            else
                config.AppSettings.Settings[key].Value = value;

            Save(config);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        static void Save(Configuration config)
        {
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}