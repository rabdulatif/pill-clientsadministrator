﻿using Pill.Admin.webmvc.Database;
using Pill.Admin.webmvc.Extensions;
using Pill.Admin.webmvc.Helpers;
using Pill.Admin.webmvc.Logics.Extensions;
using Root.viewModels.client;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Pill.Admin.webmvc.Logics.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class ClientService : IClientService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Client> GetClients()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Clients.ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Client> GetOnlineClients()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Clients.Where(s => s.IsOnline == true).AsNoTracking();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionId"></param>
        /// <returns></returns>
        public Client GetClientByConnectionId(string connectionId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Clients.FirstOrDefault(f => f.ConnectionId == connectionId);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sapCode"></param>
        /// <returns></returns>
        public Client GetClientBySapCode(string sapCode)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Clients.FirstOrDefault(f => f.SapCode == sapCode);
            }
        }

        /// <summary>
        /// Hamma clientlani Command tablitsa bilan join qilib oberadi
        /// </summary>
        /// <param name="sapCode"></param>
        /// <returns></returns>
        public List<Client> TryGetAllClientDetails()
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var result = db.Clients.Include(s => s.Commands)
                                .Include(c => c.Sessions)
                                .OrderBy(s => s.SapCode).ToList();
                    return result;
                }
                catch (Exception ex)
                {
                    LogHelper.Error(ex);
                    return null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sapCode"></param>
        /// <returns></returns>
        public Client TryGetClientDetails(string sapCode)
        {
            using (var db = new ApplicationDbContext())
            {
                try
                {
                    var result = db.Clients.Include(s => s.Commands)
                        .Include(c => c.Sessions).OrderBy(s => s.SapCode)
                        .FirstOrDefault(s => s.SapCode == sapCode);
                    return result;
                }
                catch (Exception ex)
                {
                    LogHelper.Error(ex);
                    return null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="client"></param>
        public void AddOrUpdateClient(Client client)
        {
            using (var db = new ApplicationDbContext())
            {
                var currentClient = db.Clients.FirstOrDefault(f => f.SapCode == client.SapCode);
                if (currentClient == null)
                    db.Clients.Add(client);
                else
                {
                    currentClient.ConnectionId = client.ConnectionId;
                    currentClient.IsOnline = client.IsOnline;
                    currentClient.PingTime = client.PingTime;
                    currentClient.LastSeen = client.LastSeen;
                    currentClient.RegisteredTime = client.RegisteredTime;
                }
                db.SaveChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void UpdateAllClientState(bool value)
        {
            using (var db = new ApplicationDbContext())
            {
                var query = $"Update Client SET IsOnline='{value}'";
                db.Database.ExecuteSqlCommand(query);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sapCode"></param>
        public void RemoveClient(string sapCode)
        {
            using (var db = new ApplicationDbContext())
            {
                var client = db.Clients.FirstOrDefault(s => s.SapCode == sapCode);
                if (client == null)
                    return;

                client.IsOnline = false;
                db.SaveChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientSapCode"></param>
        /// <param name="value"></param>
        public void UpdateClientState(string clientSapCode, bool value)
        {
            using (var db = new ApplicationDbContext())
            {
                var client = db.Clients.FirstOrDefault(s => s.SapCode == clientSapCode);
                if (client == null)
                    return;

                client.IsOnline = value;
                db.SaveChanges();
            }
        }
    }
}