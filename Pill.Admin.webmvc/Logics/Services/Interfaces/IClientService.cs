﻿using Pill.Admin.webmvc.Database;
using Pill.Admin.webmvc.Models;
using System.Collections.Generic;

namespace Pill.Admin.webmvc.Logics.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface IClientService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<Client> GetClients();
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<Client> GetOnlineClients();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionId"></param>
        /// <returns></returns>
        Client GetClientByConnectionId(string connectionId);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sapCode"></param>
        /// <returns></returns>
        Client GetClientBySapCode(string sapCode);
        /// <summary>
        /// /
        /// </summary>
        /// <param name="sapCode"></param>
        /// <param name="adminGuid"></param>
        /// <returns></returns>
        List<Client> TryGetAllClientDetails();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sapCode"></param>
        /// <returns></returns>
        Client TryGetClientDetails(string sapCode);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="client"></param>
        void AddOrUpdateClient(Client client);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        void UpdateAllClientState(bool value);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sapCode"></param>
        void RemoveClient(string sapCode);

        void UpdateClientState(string clientSapCode, bool v);

    }
}
