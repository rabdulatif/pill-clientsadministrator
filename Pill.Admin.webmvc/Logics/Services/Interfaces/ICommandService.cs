﻿using Pill.Admin.webmvc.Database;
using System;
using System.Collections.Generic;

namespace Pill.Admin.webmvc.Logics.Services.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICommandService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<Command> GetCommands();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        Command GetCommand(Guid guid);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        void AddEdit(Command command);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="client"></param>
        void AddCommandClient(Command cmd, Client client);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commandGuid"></param>
        void Remove(Guid commandGuid);
    }
}