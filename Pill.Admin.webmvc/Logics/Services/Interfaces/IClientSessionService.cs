﻿using Pill.Admin.webmvc.Database;
using System;
using System.Collections.Generic;

namespace Pill.Admin.webmvc.Logics.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface IClientSessionService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<Session> GetSessions();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userGuid"></param>
        /// <returns></returns>
        Session GetUserSession(Guid userGuid);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        void AddOrUpdateSession(Session session);

    }
}
