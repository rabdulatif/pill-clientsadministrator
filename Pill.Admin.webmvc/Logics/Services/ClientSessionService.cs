﻿using Pill.Admin.webmvc.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pill.Admin.webmvc.Logics.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class ClientSessionService : IClientSessionService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Session> GetSessions()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Sessions.ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userGuid"></param>
        /// <returns></returns>
        public Session GetUserSession(Guid userGuid)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Sessions.Find(userGuid);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        public void AddOrUpdateSession(Session session)
        {
            if (session.ClientGuid == Guid.Empty)
                throw new Exception("User is empty for add or update session");
            using (var db = new ApplicationDbContext())
            {
                var userSession = db.Sessions.Find(session.ClientGuid);
                if (userSession == null)
                    db.Sessions.Add(session);
                else
                {
                    userSession.Client = session.Client;
                    userSession.ClientGuid = session.ClientGuid;
                    userSession.RegisteredTime = session.RegisteredTime;
                    userSession.DisconnectTime = session.DisconnectTime;
                }
                db.SaveChanges();
            }
        }
    }
}