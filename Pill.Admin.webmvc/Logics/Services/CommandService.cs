﻿using Pill.Admin.webmvc.Database;
using Pill.Admin.webmvc.Logics.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pill.Admin.webmvc.Logics.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class CommandService : ICommandService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Command> GetCommands()
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Commands.ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public Command GetCommand(Guid guid)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Commands.FirstOrDefault(s => s.CommandGuid == guid);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        public void AddEdit(Command command)
        {
            using (var context = new ApplicationDbContext())
            {
                var currCommand = context.Commands.
                                    FirstOrDefault(f => f.CommandGuid == command.CommandGuid
                                    && f.AdminGuid == command.AdminGuid);
                if (currCommand == null)
                    context.Commands.Add(command);
                else
                {
                    currCommand.CommandName = command.CommandName;
                    currCommand.CommandQuery = command.CommandQuery;
                    currCommand.Result = command.Result;
                    currCommand.SendTime = command.SendTime;
                    currCommand.ExecutedTime = command.ExecutedTime;
                    currCommand.Status = command.Status;
                }
                context.SaveChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="client"></param>
        public void AddCommandClient(Command cmd,Client client)
        {
            using (var context = new ApplicationDbContext())
            {
                context.Commands.Attach(cmd);
                context.Clients.Attach(client);

                cmd.Clients.Add(client);

                context.SaveChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commandGuid"></param>
        public void Remove(Guid commandGuid)
        {
            if (commandGuid == Guid.Empty)
                return;
            using (var context = new ApplicationDbContext())
            {
                var command = context.Commands.FirstOrDefault(s => s.CommandGuid == commandGuid);
                if (command == null)
                    return;

                context.Commands.Remove(command);
                context.SaveChanges();
            }
        }
    }
}