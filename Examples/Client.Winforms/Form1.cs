﻿using System;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using PharmacySignalR.Client;
using Root;

namespace Client.Winforms
{
    /// <summary>
    /// 
    /// </summary>
    public partial class Form1 : Form
    {
        private IConnectionBuilder _connectionBuilder;

        /// <summary>
        /// 
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            InternalInitialize("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IlQwMDEiLCJwb3NJbmZvR3VpZCI6ImFiN2FjNzNlLTZmNzAtNGRiZS1hNjEyLTc0NzE3YjAyMjQ2MiIsIm5iZiI6MTU5MDY2NzE4MSwiZXhwIjoxNjIyMjAzMTgxLCJpYXQiOjE1OTA2NjcxODF9.IwuDZ_P4TRgpfTkjMDEDKgJv01kdtR6UhuyWSPo5Amc");
        }

        /// <summary>
        /// 
        /// </summary>
        private void InternalInitialize(string token)
        {
            _connectionBuilder = new ConnectionBuilder();
            _connectionBuilder
                .SetEndpointUrl(ConfigurationManager.AppSettings["ApiAddress"])
                .SetToken(token)
                .SetConnectionString(ConfigurationManager.ConnectionStrings["ClientDB"].ConnectionString)
                .SetHubName("HubConnectionAPI")
                .SetClient(MakeClient());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private ClientViewModel MakeClient()
        {
            return new ClientViewModel
            {
                SapCode = ConfigurationManager.AppSettings["SapCode"] ?? string.Empty
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                var result = await _connectionBuilder.ConnectAsync();
                WriteToLog(result);
            }
            catch (Exception ex)
            {
                WriteToLog(ex.Message);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        private void WriteToLog(string log)
        {
            if (InvokeRequired)
                BeginInvoke(new Action(() => txtLog?.AppendText(log + Environment.NewLine)));
            else
                txtLog?.AppendText(log + Environment.NewLine);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                _connectionBuilder.Disconnect();
                WriteToLog("Client disconnected");
            }
            catch (Exception exception)
            {
                WriteToLog(exception.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            _connectionBuilder.Disconnect();
            base.OnFormClosing(e);
        }

        private async void btnTest_Click(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < 100; i++)
                {
                    _connectionBuilder = new ConnectionBuilder();
                    _connectionBuilder
                        .SetEndpointUrl(ConfigurationManager.AppSettings["ApiAddress"])
                        .SetToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IlQwMDEiLCJwb3NJbmZvR3VpZCI6ImFiN2FjNzNlLTZmNzAtNGRiZS1hNjEyLTc0NzE3YjAyMjQ2MiIsIm5iZiI6MTU5MDY2NzE4MSwiZXhwIjoxNjIyMjAzMTgxLCJpYXQiOjE1OTA2NjcxODF9.IwuDZ_P4TRgpfTkjMDEDKgJv01kdtR6UhuyWSPo5Amc")
                        .SetConnectionString(ConfigurationManager.ConnectionStrings["ClientDB"].ConnectionString)
                        .SetHubName("HubConnectionAPI")
                        .SetClient(new ClientViewModel { SapCode = $"T0{i}" });
                    var result = await _connectionBuilder.ConnectAsync();
                    WriteToLog(result);
                    Thread.Sleep(400);
                }
            }
            catch (Exception ex)
            {
                WriteToLog(ex.Message);
            }
        }
    }
}
