﻿using Pill.Adminka.Logics.Services.Interfaces;
using Root;
using System.Collections.Generic;
using System.Timers;

namespace Pill.Adminka.Hubs
{
    /// <summary>
    /// 
    /// </summary>
    public partial class HubConnectionAPI
    {
        /// <summary>
        /// 
        /// </summary>
        private Timer timer;

        /// <summary>
        /// 
        /// </summary>
        private readonly IClientService _clientService;

        /// <summary>
        /// 
        /// </summary>
        private readonly IClientSessionService _sessionService;

        /// <summary>
        /// 
        /// </summary>
        public static List<AdminInfo> Admins { get; } = new List<AdminInfo>();

        /// <summary>
        /// 
        /// </summary>
        public static List<ClientViewModel> ClientList { get; private set; } = new List<ClientViewModel>();
    }
}