﻿using System;
using Root;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using System.Web.Mvc;
using Microsoft.AspNet.SignalR;
using Pill.Adminka.Extensions;
using Pill.Adminka.Helpers;
using Pill.Adminka.Logics.Database.Tables;
using Pill.Adminka.Logics.Extensions;
using Pill.Adminka.Logics.Services;

namespace Pill.Adminka.Hubs
{
    /// <summary>
    /// 
    /// </summary>
#if !DEBUG
    [Microsoft.AspNet.SignalR.Authorize()]
#else
    [AllowAnonymous]
#endif
    public partial class HubConnectionAPI : Hub
    {
        private static Boolean IsInitialized = false;

        public HubConnectionAPI()
        {
            _clientService = new ClientService();
            _sessionService = new ClientSessionService();

            InitializeClientList();
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitializeClientList()
        {
            try
            {
                if (IsInitialized)
                    return;
                if (ClientList.Count == 0)
                    _clientService.UpdateAllClientState(false);
                var clients = _clientService.GetClients();
                ClientList.AddRange(clients.ToClientViewModels());

                IsInitialized = true;
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override Task OnReconnected()
        {
            try
            {
                UpdateOnReconnected();
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex);
            }
            return base.OnReconnected();
        }

        /// <summary>
        /// 
        /// </summary>
        private void UpdateOnReconnected()
        {
            var client = ClientList.Find(s => s.ConnectionId == Context.ConnectionId);
            if (client != null)
            {
                ClientList.Remove(client);

                client.LastSeen = DateTime.Now;
                client.PingTime = DateTime.Now;
                ClientList.Add(client);
                AddOrUpdateUserInDb(client.ToUser());
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stopCalled"></param>
        /// <returns></returns>
        public override Task OnDisconnected(bool stopCalled)
        {
            try
            {
                DisconnectHubClient(stopCalled);
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex);
            }

            return base.OnDisconnected(stopCalled);
        }

        private void DisconnectHubClient(bool stopCalled)
        {
            DisconnectIfAdmin();
            DisconnectFromAdminIfClient();
        }

        /// <summary>
        /// 
        /// </summary>
        private void DisconnectIfAdmin()
        {
            Admins.RemoveAll(a => a.ConnectionId == Context.ConnectionId);
        }

        /// <summary>
        /// 
        /// </summary>
        private void DisconnectFromAdminIfClient()
        {
            var client = ClientList.Find(s => s.ConnectionId == Context.ConnectionId);
            if (client != null)
            {
                ClientList.SetClientState(client.SapCode, false);
                _clientService.RemoveClient(client.SapCode);

                var dbClient = _clientService.GetClientBySapCode(client.SapCode);
                if (dbClient == null)
                    return;
                _sessionService.AddOrUpdateSession(new ClientSession
                {
                    RegisteredTime = client.RegisteredTime,
                    LastConnectedEndTime = client.LastSeen,
                    UserGuid = dbClient.ClientGuid
                });

                Clients.All.adminOnDisconnected(client.SapCode);
            }
        }

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="client"></param>
        public void Register(ClientViewModel client)
        {
            if (String.IsNullOrEmpty(client.SapCode))
                return;
            client.ConnectionId = Context.ConnectionId;
            client.RegisteredTime = DateTime.Now;
            client.PingTime = DateTime.Now;
            client.LastSeen = DateTime.Now;
            client.IsOnline = true;

            ClientList.RemoveAll(r => r.SapCode == client.SapCode);
            ClientList.Add(client);

            AddOrUpdateUserInDb(client.ToUser());

            Ping(); //TODO: Delete this line

            Clients.All.adminOnConnected(client);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="guid"></param>
        public void AdminRegister(string guid)
        {
            var connectInfo = new AdminInfo
            {
                ConnectionId = Context.ConnectionId,
                AdminGuid = guid
            };

            Admins.RemoveAll(a => a.AdminGuid == guid);
            Admins.Add(connectInfo);
        }

        public void OnlineClients()
        {
            Clients.All.onlineClients(ClientList);
        }

        #region PingClients

        public void Ping()
        {
            PingClient();
        }

        private void PingClient()
        {
            if (timer != null)
                return;
            timer = new Timer(15000);
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        private async void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            await InternalOnlineClients();
        }

        private async Task InternalOnlineClients()
        {
            try
            {
                if (ClientList.Count > 0)
                {
                    ClientList.ForEach((client) => PongClients(ref client));
                    await Task.Delay(10000);
                    ClientList.ForEach((client) => TryCheckPingTimeout(ref client));
                    OnlineClients();
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex);
            }
        }

        public void PongClients(ref ClientViewModel client)
        {
            ClientList.SetClientPingTime(client.SapCode, DateTime.Now);
            Clients.Client(client.ConnectionId).Pong();
            AddOrUpdateUserInDb(client.SapCode);
        }

        private void TryCheckPingTimeout(ref ClientViewModel client)
        {
            var diffInSeconds = (client.PingTime - client.LastSeen).TotalSeconds;
            if (Math.Abs(diffInSeconds) > 10 && client.IsOnline == true)
            {
                ClientList.SetClientState(client.SapCode, false);
                AddOrUpdateUserInDb(client.SapCode);
            }
        }

        #endregion


        /// <summary>
        /// 
        /// </summary>
        public void OnPong(string sapCode)
        {
            try
            {
                ClientList.SetClientLastActiveTime(sapCode, DateTime.Now);
                ClientList.SetClientState(sapCode, true);
                AddOrUpdateUserInDb(sapCode);
            }
            catch (Exception ex)
            {
                LogHelper.Error(ex);
            }
        }

        private void AddOrUpdateUserInDb(string sapCode)
        {
            var client = ClientList.FirstOrDefault(s => s.SapCode == sapCode);
            if (client == null)
                return;

            _clientService.AddOrUpdateClient(client.ToUser());
        }

        private void AddOrUpdateUserInDb(Client client)
        {
            if (client == null)
                return;
            _clientService.AddOrUpdateClient(client);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionIds"></param>
        /// <param name="cmd"></param>
        public void SendCommandToClients(List<string> connectionIds, Command cmd)
        {
            //_commandService.AddOrUpdateCommand(cmd);
            var commandViewModel = cmd.ToCommandViewModel();
            Clients.Clients(connectionIds).serverCommand(commandViewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commandResult"></param>
        public void ClientMessage(CommandResultViewModel commandResult)
        {
            var admin = Admins.FirstOrDefault(a => a.AdminGuid == commandResult.AdminGuid);
            if (admin == null) return;

            Clients.Client(admin.ConnectionId).adminOnClientMessageReceived(commandResult.Content);
        }
        #endregion
    }
}