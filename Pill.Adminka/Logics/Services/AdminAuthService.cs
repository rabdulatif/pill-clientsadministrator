﻿using Pill.Adminka.Logics.Database;
using Pill.Adminka.Logics.Database.Tables;
using Pill.Adminka.Logics.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pill.Adminka.Logics.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class AdminAuthService : IAdminAuthService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AdminAuth> GetAdmins()
        {
            using (var db = new ClientsContext())
            {
                return db.AdminAuth.ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="adminGuid"></param>
        /// <returns></returns>
        public AdminAuth GetAdmin(Guid adminGuid)
        {
            using (var db = new ClientsContext())
            {
                return db.AdminAuth.FirstOrDefault(f => f.AdminGuid == adminGuid);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sapCode"></param>
        /// <returns></returns>
        public AdminAuth GetAdmin(string sapCode)
        {
            using (var db = new ClientsContext())
            {
                return db.AdminAuth.FirstOrDefault(f => f.SapCode == sapCode);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="adminAuth"></param>
        public void AddEdit(AdminAuth adminAuth)
        {
            using (var db = new ClientsContext())
            {
                var curAdmin = db.AdminAuth.FirstOrDefault(s => s.SapCode == adminAuth.SapCode);
                if (curAdmin == null)
                    db.AdminAuth.Add(adminAuth);
                else
                {
                    curAdmin.HardwareID = adminAuth.HardwareID;
                    curAdmin.AppVersion = adminAuth.AppVersion;
                    curAdmin.Password = adminAuth.Password;
                }
                db.SaveChanges();
            }
        }
    }
}