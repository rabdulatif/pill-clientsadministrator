﻿using Pill.Adminka.Logics.Database;
using Pill.Adminka.Logics.Database.Tables;
using Pill.Adminka.Logics.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pill.Adminka.Logics.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class ClientSessionService : IClientSessionService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ClientSession> GetSessions()
        {
            using (var db = new ClientsContext())
            {
                return db.Sessions.ToList();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userGuid"></param>
        /// <returns></returns>
        public ClientSession GetUserSession(Guid userGuid)
        {
            using (var db = new ClientsContext())
            {
                return db.Sessions.Find(userGuid);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        public void AddOrUpdateSession(ClientSession session)
        {
            if (session.UserGuid == Guid.Empty)
                throw new Exception("User is empty for add or update session");
            using (var db = new ClientsContext())
            {
                var userSession = db.Sessions.Find(session.UserGuid);
                if (userSession == null)
                    db.Sessions.Add(session);
                else
                {
                    userSession.User = session.User;
                    userSession.UserGuid = session.UserGuid;
                    userSession.RegisteredTime = session.RegisteredTime;
                    userSession.LastConnectedEndTime = session.LastConnectedEndTime;
                }
                db.SaveChanges();
            }
        }
    }
}