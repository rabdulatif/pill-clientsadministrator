﻿using Pill.Adminka.Logics.Database.Tables;
using System;
using System.Collections.Generic;

namespace Pill.Adminka.Logics.Services.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAdminAuthService
    {
        IEnumerable<AdminAuth> GetAdmins();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SapCode"></param>
        /// <returns></returns>
        AdminAuth GetAdmin(string SapCode);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="adminGuid"></param>
        /// <returns></returns>
        AdminAuth GetAdmin(Guid adminGuid);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="credentials"></param>
        void AddEdit(AdminAuth credentials);
    }
}
