﻿

using Pill.Adminka.Logics.Database.Tables;
using System;
using System.Collections.Generic;

namespace Pill.Adminka.Logics.Services.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IClientSessionService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<ClientSession> GetSessions();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userGuid"></param>
        /// <returns></returns>
        ClientSession GetUserSession(Guid userGuid);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        void AddOrUpdateSession(ClientSession session);

    }
}
