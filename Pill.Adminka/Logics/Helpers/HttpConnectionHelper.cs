﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Pill.Adminka.Logics.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class HttpConnectionHelper
    {
        /// <summary>
        /// Путь к API 
        /// </summary>
        private static string _apiAddress;

        public static HttpClient GetHttpClient()
        {

#if !DEBUG
            const string address = "http://api.pill.uz:4950";
#endif

#if DEBUG
            var address = ConfigurationManager.AppSettings["HttpClientAddress"];
#endif
            _apiAddress = address;

            var client = new HttpClient { BaseAddress = new Uri(_apiAddress) };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }
    }
}