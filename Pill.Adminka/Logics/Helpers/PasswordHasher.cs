﻿using System;
using System.Text;

namespace Pill.Adminka.Logics.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public static class PasswordHasher
    {
        public static string Hash(string value)
        {
            return Convert.ToBase64String(System.Security.Cryptography.SHA256.Create()
                .ComputeHash(Encoding.UTF8.GetBytes(value)));
                
        }
    }
}