﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pill.Adminka.Logics.Database.Tables
{
    public class Command
    {
        public Command()
        {
            Clients = new HashSet<Client>();
        }

        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Browsable(false)]
        public Guid CommandGuid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AdminGuid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CommandName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CommandQuery { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? IsSuccess { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual ICollection<Client> Clients { get; set; }
    }
}