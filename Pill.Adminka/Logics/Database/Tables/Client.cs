﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pill.Adminka.Logics.Database.Tables
{
    /// <summary>
    /// 
    /// </summary>
    [Table("Client")]
    public class Client
    {
        /// <summary>
        /// 
        /// </summary>
        public Client()
        {
            Sessions = new HashSet<ClientSession>();
        }

        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ClientGuid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ConnectionId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsOnline { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SapCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime PingTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime LastSeen { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime RegisteredTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ICollection<ClientSession> Sessions { get; set; }
    }
}