﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pill.Adminka.Logics.Database.Tables
{
    /// <summary>
    /// 
    /// </summary>
    [Table("ClientSession")]
    public class ClientSession
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid SessionGuid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid UserGuid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Client User { get; set; }

        /// <summary>
        /// Pегистрированное время
        /// </summary>
        public DateTime RegisteredTime { get; set; }

        /// <summary>
        /// Время завершения последнего подключения
        /// </summary>
        public DateTime LastConnectedEndTime { get; set; }

        /// <summary>
        /// Продолжительность сеанса
        /// </summary>
        public string SessionLenght =>
               TimeSpan.FromMinutes((RegisteredTime - LastConnectedEndTime).TotalMinutes).ToString(@"hh\:mm\:ss");
    }

}