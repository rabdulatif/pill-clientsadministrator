﻿using Pill.Adminka.Logics.Database.Tables;
using Pill.Adminka.Migrations;
using System.Data.Entity;

namespace Pill.Adminka.Logics.Database
{
    /// <summary>
    /// 
    /// </summary>
    public class ClientsContext : DbContext
    {
        /// <summary>
        /// 
        /// </summary>
        public ClientsContext():base("name=ConnectionDb")
        {
            System.Data.Entity.Database.SetInitializer(new MigrateDatabaseToLatestVersion<ClientsContext,Configuration>());
        }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<Client> Clients { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<ClientSession> Sessions { get; set; }
    }
}