﻿using System.ComponentModel.DataAnnotations;

namespace Pill.Adminka.Logics.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class Credentials
    {
        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Сап-код")]
        //[Required]
        public string SapCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Display(Name = "Пароль")]
        //[Required]
        public string Password { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string HardwareId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AppVersion { get; set; }
    }
}