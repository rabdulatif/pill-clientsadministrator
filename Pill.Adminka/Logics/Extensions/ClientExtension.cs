﻿using Pill.Adminka.Logics.Database.Tables;
using Root;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pill.Adminka.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static  class ClientExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clients"></param>
        /// <returns></returns>
        public static List<ClientViewModel> ToClientViewModels(this IEnumerable<Client> clients)
        {
            List<ClientViewModel> viewModelList = new List<ClientViewModel>();

            foreach (var item in clients)
            {
                viewModelList.Add(new ClientViewModel
                {
                    ClientGuid = item.ClientGuid,
                    SapCode = item.SapCode,
                    ConnectionId = item.ConnectionId,
                    IsOnline = item.IsOnline,
                    PingTime = item.PingTime,
                    LastSeen = item.LastSeen,
                    RegisteredTime = item.RegisteredTime
                });
            }

            return viewModelList;
        }
    }
}