﻿using Root;
using System;
using System.Collections.Generic;

namespace Pill.Adminka.Logics.Extensions
{
    public static class ListExtension
    {
        public static void SetClientLastActiveTime(this List<ClientViewModel> clients,
                                                   string sapCode, DateTime value)
        {
            foreach (var client in clients)
            {
                if (client.SapCode == sapCode)
                    client.LastSeen = value;
            }
        }

        public static void SetClientPingTime(this List<ClientViewModel> clients, string sapCode, DateTime value)
        {
            foreach (var client in clients)
            {
                if (client.SapCode == sapCode)
                    client.PingTime = value;
            }
        }

        public static void SetClientState(this List<ClientViewModel> clients, string sapCode, bool value)
        {
            foreach (var client in clients)
            {
                if (client.SapCode == sapCode)
                    client.IsOnline = value;
            }
        }
    }
}