﻿using Pill.Adminka.Logics.Database.Tables;
using Root;

namespace Pill.Adminka.Logics.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class CommandExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public static CommandViewModel ToCommandViewModel(this Command command)
        {
            return new CommandViewModel
            {
                CommandGuid = command.CommandGuid,
                CommandQuery = command.CommandQuery,
                AdminGuid = command.AdminGuid
            };
        }
    }
}