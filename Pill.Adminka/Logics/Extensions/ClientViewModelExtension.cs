﻿using Pill.Adminka.Logics.Database.Tables;
using Root;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pill.Adminka.Logics.Extensions
{
    /// <summary>
    /// 
    /// </summary>
    public static class ClientViewModelExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientViewModel"></param>
        /// <returns></returns>
        public static Client ToUser(this ClientViewModel clientViewModel)
        {
            return new Client
            {
                ClientGuid = clientViewModel.ClientGuid,
                ConnectionId = clientViewModel.ConnectionId,
                IsOnline = clientViewModel.IsOnline,
                SapCode = clientViewModel.SapCode,
                PingTime = clientViewModel.PingTime,
                LastSeen = clientViewModel.LastSeen,
                RegisteredTime = clientViewModel.RegisteredTime,
            };
        }
    }
}