﻿using Pill.Adminka.Logics.Helpers;
using Pill.Adminka.Logics.Models;
using System.Net.Http;

namespace Pill.Adminka.Logics.Managers
{
    /// <summary>
    /// 
    /// </summary>
    public class AuthenticationManager
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        public static HttpResponseMessage Register(Credentials credentials)
        {
            var httpClient = HttpConnectionHelper.GetHttpClient();
            var httpResponse = new HttpResponseMessage();

            httpResponse = httpClient.PostAsJsonAsync("/api/Account/register", credentials).Result;

            return httpResponse;
        }
    }
}