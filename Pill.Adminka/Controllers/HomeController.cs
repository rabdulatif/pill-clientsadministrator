﻿using Pill.Adminka.Extensions;
using Pill.Adminka.Hubs;
using Pill.Adminka.Logics.Services;
using System.Web.Mvc;

namespace Pill.Adminka.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var clients = HubConnectionAPI.ClientList;
            return Json(clients,JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Clients()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Admins()
        {
            return View();
        }
    }
}