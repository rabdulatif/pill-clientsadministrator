﻿using Pill.Adminka.Logics.Database.Tables;
using Pill.Adminka.Logics.Helpers;
using Pill.Adminka.Logics.Managers;
using Pill.Adminka.Logics.Models;
using Pill.Adminka.Logics.Services.Interfaces;
using System.Configuration;
using System.Web.Mvc;

namespace Pill.Adminka.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class AccountController : Controller
    {
        IAdminAuthService _service;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        public AccountController(IAdminAuthService service)
        {
            _service = service;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="credentials"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Register([Bind(Include = "SapCode,Password")]Credentials credentials)
        {
            bool Status = false;
            string message = "";

            if (!ModelState.IsValid)
                message = "Неверный запрос";

            credentials.HardwareId = AppSettingsHelper.GetAdminHardwareID(credentials.SapCode);
            credentials.AppVersion = ConfigurationManager.AppSettings["AppVersion"];

            var result = AuthenticationManager.Register(credentials);
            if (result.IsSuccessStatusCode)
            {
                credentials.Password = PasswordHasher.Hash(credentials.Password);
                _service.AddEdit(new AdminAuth
                {
                    AppVersion = credentials.AppVersion,
                    SapCode = credentials.SapCode,
                    Password = credentials.Password,
                    HardwareID = credentials.HardwareId
                });

                message = "Регистрация успешно выполнена";
                Status = true;
            }

            ViewBag.Message = message;
            ViewBag.Status = Status;

            return View(credentials);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Login()
        {
            return View(new Credentials());
        }
    }
}