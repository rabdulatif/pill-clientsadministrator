﻿using Ninject.Modules;
using Pill.Adminka.Logics.Services;
using Pill.Adminka.Logics.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pill.Adminka.App_Start
{
    /// <summary>
    /// 
    /// </summary>
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<IAdminAuthService>().To<AdminAuthService>();
        }
    }
}