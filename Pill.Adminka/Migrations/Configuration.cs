﻿namespace Pill.Adminka.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Logics.Database.ClientsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Pill.Adminka.Logics.Database.ClientsContext";
        }
    }
}
